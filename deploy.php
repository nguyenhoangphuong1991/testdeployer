<?php
namespace Deployer;

require 'recipe/drupal8.php';

// Project name
set('application', 'my_project');

// Project repository
//set('repository', 'git@gitlab.com:nguyenhoangphuong1991/testdeployer.git');
//
//// [Optional] Allocate tty for git clone. Default value is false.
//set('git_tty', true);

// Shared files/dirs between deploys
//add('shared_files', []);
//add('shared_dirs', []);

// Writable dirs by web server
//add('writable_dirs', []);


// Hosts

host('localhost')
    ->user('homepage')
    ->set('deploy_path', '/var/www/html/{{application}}')
    ->identityFile('~/.ssh/id_rsa');
//host('52.192.92.238')
//    ->user('homepage')
//    ->set('deploy_path', '~/home/homepage/public_html/surugaya_global/test_deploy/{{application}}');


// Tasks

task('deploy:upload', function() {
    $appFiles = [
        'test_dep.phtml',
    ];
    $deployPath = get('deploy_path');

    foreach ($appFiles as $file)
    {
        upload($file, "{$deployPath}/{$file}");
    }
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

